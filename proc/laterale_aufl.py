import numpy as np
import matplotlib.pyplot as plt
import scipy.constants as sc
import matplotlib.dates as mdates
import matplotlib.ticker as mtick

def dl(r):
    v = sc.c/3
    return np.sqrt(v*r/(2.*200e6))

if __name__ == "__main__":
    r = np.linspace(0.12,10,100)
    dl_r = dl(r)

    f,ax = plt.subplots()
    ax.plot(r,dl_r)
    ax.set_xlabel("Depth/m",size="small")
    ax.set_ylabel(u"Lateral resolution/m",size="small")
    ax.yaxis.set_major_locator(mtick.MultipleLocator(0.2))
    ax.yaxis.set_minor_locator(mtick.MultipleLocator(0.05))
    ax.xaxis.set_major_locator(mtick.MultipleLocator(1.))
    ax.xaxis.set_minor_locator(mtick.MultipleLocator(0.1))
    ax.tick_params(which="both",labelsize="small")
    ax.tick_params(which="both",labelsize="small")
    plt.grid("on",which="both")
    f.tight_layout()
    f.savefig("laterale_aufloesung.png",dpi=300,bbox_inches="tight")
    plt.show()
