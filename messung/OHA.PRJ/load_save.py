import numpy as np
from glob import glob
import h5py
import sys,os
import gpr_gssi as dzt

if __name__ == "__main__":
    files = sorted(glob("*.DZT"))
    f = dzt.hdf5.get_handle(files[0].split("_")[0])
    for ifile in files:
        header, data = dzt.gssi.readgssi(ifile, antfreq=200, stack=1)

        #save as hdf5
        dzt.hdf5.export(f,header,data)
    f.close()

    f = dzt.hdf5.get_handle(glob("*.hdf5")[0])
    keys = f.keys()
    shapes = []
    for key in keys:
        shapes.append(np.array(f[key]).shape)
        #print f[key].attrs.items()
    shapes = np.array(shapes)
    max_lines = np.max(shapes[:,1])
    da = np.array(f[keys[0]])
    da = np.array(map(dzt.zero.shift,da.T)).T
    le = max_lines-da.shape[1]
    da = np.pad(da,[(0,0),(le/2,le/2)],mode="constant").T

    from skimage import exposure
    from skimage.morphology import disk
    from skimage.filters import rank
    selem = disk(30)

    data        = np.expand_dims(da,axis=0)
    data_global = np.expand_dims(exposure.equalize_hist(da),axis=0)
    data_local  = np.expand_dims(rank.equalize(da,selem=selem),axis=0)
    for key in keys[1:]:
        da = np.array(f[key])
        da = np.array(map(dzt.zero.shift,da.T)).T
        le = max_lines-da.shape[1]
        da = np.pad(da,[(0,0),(le/2,le/2)],mode="constant").T
        data        = np.append(data,[da],axis=0)
        data_global = np.append(data_global,[exposure.equalize_hist(da)],axis=0)
        data_local  = np.append(data_local,[rank.equalize(da,selem=selem)],axis=0)

    dset = f.create_dataset("cube", data.shape,
                                    chunks=True,
                                    compression="gzip",
                                    compression_opts=9,
                                    data=data)
    dset = f.create_dataset("cube_global", data_global.shape,
                                    chunks=True,
                                    compression="gzip",
                                    compression_opts=9,
                                    data=data_global)
    dset = f.create_dataset("cube_local", data_local.shape,
                                    chunks=True,
                                    compression="gzip",
                                    compression_opts=9,
                                    data=data_local)
    f.close()

